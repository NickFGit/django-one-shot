from django.urls import path
from todos.views import (list_todos, todo_details, create_todo,
                         update, delete_list, update_item,
                         create_todo_item)


urlpatterns = [
    path("", list_todos, name="todo_list_list"),
    path("<int:id>/", todo_details, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", update, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
]
