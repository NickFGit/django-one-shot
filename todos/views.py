from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def list_todos(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "todos/list.html", context)


def todo_details(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo": todo
    }
    return render(request, "todos/details.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid:
            list = form.save()
            return redirect(todo_details, id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create_list.html", context)


def update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid:
            list = form.save()
            return redirect(todo_details, id=list.id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "form": form
    }
    return render(request, "todos/edit_list.html", context)


def delete_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect(list_todos)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "form": form
    }
    return render(request, "todos/delete_list.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid:
            item = form.save()
            return redirect(todo_details, id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/create_item.html", context)


def update_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid:
            item = form.save()
            return redirect(todo_details, id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "form": form
    }
    return render(request, "todos/edit_item.html", context)
